//將 3 和 5 的倍數加總起來
/* 
從 0 到 10 ，3 的倍數把他們加總
從 0 到 10 ，5 的倍數把他們加總
*/

console.log(sum(15));
function sum(limit){
    let sum = 0 ; 
    for(let i=0 ; i <= limit; i++)
        if (i % 3 === 0 || i % 5 ===0)
            sum += i;
    return sum
}


sumThreeAndFive(12);

function sumThreeAndFive(limitNum){
    let sum = 0 ;
    
    for ( let i = 1 ; i <= limitNum ; i++ ){
    
        if ( i % 3 === 0 ){
            sum += i;
        } 
    
        if ( i % 5 === 0 ){
            sum += i;
        }
    
    }
    
    console.log(sum);
};