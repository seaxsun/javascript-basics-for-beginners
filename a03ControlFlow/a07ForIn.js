const person = {
    name : 'Mosh' ,
    age : 30
};

for ( let key in person )
    console.log( key, person[key] ) ;
//name Mosh
//age 30

// dot Noattion
person.name

// Bracket Notation
person['name']

const colors = ['red', 'green', 'blue'];

for (let abc in colors)
    console.log( abc, colors[abc]);
//0 red
//1 green
//2 blue
