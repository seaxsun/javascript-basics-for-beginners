// Hour
// If hour is between 6am and 12pm: Good morning!
// 6點到12點之間，顯示 Good Morning
// If it is between 12pm and 6pm: Good afternoon!
// 晚上12點到下午午六點就顯示 afternoon
// Otherwise: Good evening!
// 其他情況：good evening

let hour = 20;

if (hour >= 6 && hour < 12) 
    console.log('Good morning!!');
else if (hour >= 12 && hour < 18) 
    console.log('Godd afternoon!!');
else
    console.log('Good evening!!');

/* 
if (condition) {
    statement
}
else if (anotherCondition) {
    statement
}
else if (yetAnotherCondition) 
    statement
else
    statement */