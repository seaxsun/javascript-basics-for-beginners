//打印 1~10，遇到5就跳過
let i = 0;
while (i <= 8) {
    if (i === 5) {
        i++;
        continue;
    }
    console.log(i)
    i++;
}

//break 是符合條件，打斷，跳出迴圈
//continue 是符合條件，跳過這次迴圈