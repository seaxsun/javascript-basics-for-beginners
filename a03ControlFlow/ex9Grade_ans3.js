const marks = [60, 70, 100];

console.log(calculateAverage(marks));
console.log(calculateGrade(marks));

function calculateGrade(marks) {
    const average = calculateAverage(marks);
    if (average < 60) return 'F';
    if (average < 70) return 'D';
    if (average < 80) return 'C';
    if (average < 90) return 'B';
    //return 'A' （作者原句是只有 return）
    return (average <= 100 ) ? 'A': 'liar';
}

function calculateAverage(array) {
    let sum = 0 ;
    for ( mark of array)
        sum += mark;
    return Math.floor(sum / array.length);
}

