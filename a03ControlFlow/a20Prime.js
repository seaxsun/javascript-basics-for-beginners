//程式目的是找出質數
/* 
5除2 不能整數
5就是餘數
10可以被2整除
就拿給3整除
3不能整除
10就是餘數

5除3 不能整數
5就是質數
8除2 可以整除
8除3 不可以整除
其中之一可以整除就是因數

如果 4 被 2除 不等於零
    所以是質數

*/
/* 
let i = 17 ; 
if (i % 2 !== 0){
    if ( i % 3 !== 0 ) {
        console.log(i, '= 質數');
    }else {
        console.log(i, '= 因數');
    }
} else {
    console.log(i, '= 因數');
}; */


showPrimes(40);
function showPrimes(limits){
    const evenNum = 2;
    const oddNum = 3; 
    for (let i = 2 ; i < limits ; i++){
        if (i % 2 !== 0){
            if ( i % 3 !== 0 ) {
                console.log(i, '= 質數');
            }
        }
    }
}
