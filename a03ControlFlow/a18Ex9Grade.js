// 加總分數，平均，分級
// 把功能簡單化，拆分二個
const 分數 = [80, 100, 100];
分級(分數);
//功能1
function 分級(分數){
    const 平均 = 計算平均(分數);
    if ( 平均 < 60 ) return console.log('F');
    if ( 平均 < 70) return console.log('D');
    if ( 平均 < 80) return console.log('C');
    if ( 平均 < 90) return console.log('B');
    return console.log('A');
}
//功能2
function 計算平均(分數){
    let 加總 = 0 ;
    for (let i of 分數){
        加總 += i;
    }
    return Math.floor(加總 / 分數.length);
}

