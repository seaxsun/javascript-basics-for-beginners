// 速限是70
// 每超過 5 扣一分
// 扣滿 12分 就暫停駕照
// 顯示[1]速限正常 [2]暫停駕照 [3]扣分
carSpeed( 75 );
function carSpeed(speed) {
    const speedLimit = 70 ; 
    const kmPerPoint = 5 ;
    if (speed < speedLimit + kmPerPoint ) {
        console.log('ok')
        return;
    }

    const point = Math.floor((speed - speedLimit) /5);
    if (point >= 12)
        console.log( 'License suspended');
    else
        console.log('point', point);
}
//有else 的寫法
/* 
carSpeed( 75 );

function carSpeed(speed) {
    const speedLimit = 70 ; 
    const kmPerPoint = 5 ;
    if (speed < speedLimit + kmPerPoint )
        console.log('ok')
    else {
        const point = Math.floor((speed - speedLimit) /5);
        if (point >= 12)
            console.log( 'License suspended');
        else
            console.log('point', point);
    }
} */