//打印 obj 的屬性
//只有string才能被打印出來
const movie = {
    title : 'a',
    releaseYear : 2018,
    rating : 4.5,
    director : 'b'
};

showProperties(movie);
function showProperties(obj){
    for (let i in obj)
        if ( typeof(obj[i]) === 'string') console.log(i, obj[i]);
}
// title a
// director b

