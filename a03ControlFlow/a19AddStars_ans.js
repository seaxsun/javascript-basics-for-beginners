showStars(10);

function showStars(rows) {
    if (rows === 0) {
        console.log('No Zero')
    } else {
        for (let row = 1; row <= rows; row++) {
            let patten = '';
            for (let i = 1; i <= row; i++) {
                patten += "*";
            }
            console.log(patten);
        }
    }
}
