// 被3整除 輸出 Fizz
// 被5整除 輸出 Buzz
// 被3和5整除 輸出 FizzBuzz
// 不被 3和 5整除 輸出原數字
// 不是數字就輸出非數字
const output = fizzBuzz(15);
console.log(output);
function fizzBuzz(input) {
    if (typeof(input) !== 'number')
        return 'Not a number' ;
    if ((input % 3 === 0) && (input % 5 === 0))
        return 'FizzBuzz' ; 
    if (input % 3 === 0)
        return 'Fizz' ;
    if (input % 5 === 0)
        return 'Buzz' ; 
    return input ; 
}
//FizzBuzz

