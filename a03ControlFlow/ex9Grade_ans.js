const marks = [0, 100, 100];

console.log(calculateGrade(marks));
function calculateGrade(marks) {
    let sum = 0;
    for (mark of marks) {
        sum += mark;
    }
    let average = Math.floor(sum / marks.length);
    console.log(average);
    if (average >= 0 && average <= 59) return 'F';
    if (average >= 60 && average <= 69) return 'E';
    if (average >= 70 && average <= 79) return 'D';
    if (average >= 80 && average <= 89) return 'C';
    if (average >= 90 && average <= 99) return 'B';
    if (average == 100) return 'A';
    if (average < 0 || average > 100) return 'liar';
}