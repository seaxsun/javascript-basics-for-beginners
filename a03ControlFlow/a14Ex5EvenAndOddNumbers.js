showNumber(5);
function showNumber(limit){
    for (let i = 0 ; i <=limit ; i++){
        if (i % 2 === 0) console.log(i, '"EVEN"');
        if (i % 2 !== 0) console.log(i, '"OOD"'); 
    }
}

showNumber2(5);
function showNumber2(limit2){
    for (let i = 0 ; i <= limit2 ; i++ ){
        const message = ( i % 2 === 0 ) ? 'EVEN' : 'ODD'; 
        console.log(i, message);
    }
}

