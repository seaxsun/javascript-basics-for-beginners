//do-while 
let i = 9;
do{
    if (i % 2 !== 0 ) console.log(i);
    i++;
}while(i <= 5);
// 9
// do-while 至少執行一次，就算條件不對
// while 則是看條件

