const marks = [1000, 100, 100];

console.log(calculateGrade(marks));
function calculateGrade(marks) {
    let sum = 0;
    for (mark of marks) {
        sum += mark;
    }
    let average = Math.floor(sum / marks.length);
    console.log(average);
    if (average < 60) return 'F';
    if (average < 70) return 'D';
    if (average < 80) return 'C';
    if (average < 90) return 'B';
    //return 'A' （作者原句是只有 return）
    return (average <= 100 ) ? 'A': 'liar';
}