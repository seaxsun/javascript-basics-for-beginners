// 物件 object
let person = {
    name : 'Mosh',
    age : 30
};

// Dot Notation
person.name = 'John' ; 
console.log(person.name)
//John

// Bracket Notation
person['age'] = 300;
let seleection = 'name';
person[seleection] = 'Mary'
console.log(person[seleection]);
//Mary
console.log(person['age']);
//300

