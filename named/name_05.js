
const  filterUnderLine = (obj, char = '_') => {
     const arr =  Object.keys(obj).filter(item => item.indexOf(char) !== -1)
     arr.forEach(item => {
       const before = obj[item]
       const key = replaceUnderLine(item)
       obj[key] = before
       delete obj[item]
     })
     return obj
   }
   console.log(filterUnderLine({test_name: 'frank'})) // { testName: 'frank }