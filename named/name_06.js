
// 方法1：采用數組的方法
function getKebabCase1(str){
    var arr=str.split('')
    str=arr.map(function(item){
        if(item.toUpperCase()===item){
            return '-'+item.toLowerCase();
        }else{
            return item;
        }
    })
    return str.join( '' )
}
console.log(getKebabCase1('userName')) //user-name

//換回去
function getCamelCase1(str) {
    var arr=str.split('-')
    return arr.map(function(item,index){
        if(index===0){
            return item
        }else{
            return item[0].toUpperCase()+item.slice(1)
        }
    }).join('')
}
console.log( getCamelCase1( 'user-name' ) ); //userName


//方法2：采用正則表達式

function getKebabCase2(str){
    return str.replace(/[A-Z]/g, function(item) {
        return '-'+item.toLowerCase()
    })
}
console.log( getKebabCase2( 'userName' ) ) //user-name

//換回去
function getCamelCase2(str) {
    return str.replace(/-([a-z])/g,function(keb,item){
        return item.toUpperCase();
    } )
}
console.log( getCamelCase2( 'user-name' ) ); //userName