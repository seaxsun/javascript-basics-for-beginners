
const replaceUnderLine = (val, char = '_') => {
     const arr = val.split('')
     const index = arr.indexOf(char)
     arr.splice(index, 2, arr[index+1].toUpperCase())
     val = arr.join('')
     return val
   }
   console.log(replaceUnderLine('test_prop')) // testProp