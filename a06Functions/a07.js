const person4 = {
    firstName: 'Mosh',
    lastName: 'Hamedani',
    get fullName(){
        return `${this.firstName} ${this.lastName}` ; 
    },
    set fullName(value){
        if (typeof value !== 'string')
            throw new Error('Value is not a String.');
        const parts = value.split(' ');
        if (parts.length !== 2)
            throw new Error('Enter a first a last name.')

        this.firstName = parts[0];
        this.lastName = parts[1];
    }
};

try {
    person4.fullName = null ; // 不可以輸入空值
    person4.fullName = 'joe' ; //單一個名字 joe 少了姓氏

} 
catch (error) {
    console.log(error)
}