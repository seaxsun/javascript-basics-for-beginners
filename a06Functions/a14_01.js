try {
    const numbers = [1,1,1,1,1,2,3,4];
    const count = count0ccurrences(null, 1);
    console.log(count);
} catch (error) {
    console.log(error.message);
}

//這裡要寫「 錯誤處理 」
function count0ccurrences(array, searchElement){
    if(!Array.isArray(array))
        throw new Error('Invalid array')

    return array.reduce((sum, n)=>{
        const occurrence = (n === searchElement)? 1:0
        return sum + occurrence;
    },0)
}
