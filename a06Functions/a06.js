//這裡加了 get 和 set 
//這裡已經是物件導向
//與建構函數的樣子不同
const person = {
    firstName: 'Mosh',
    lastName: 'Hamedani',
    get fullName(){
        return `${this.firstName} ${this.lastName}` ; 
    },
    set fullName(value){
        const parts = value.split(' ');
        this.firstName = parts[0];
        this.lastName = parts[1];
    }
};
person.fullName = 'jung lu';
console.log(person.fullName);
// jung lu
