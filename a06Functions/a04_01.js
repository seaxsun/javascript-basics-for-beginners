function sum(discount, ...prices){
    let total = 0;
    total = prices.reduce((a,b)=> a+b);
    console.log(total);
    return total * (1 - discount);
}

console.log(sum(0.1,10,20,30,40));
