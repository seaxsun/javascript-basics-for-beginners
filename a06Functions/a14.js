try {
    const number = [1,2,3,4];
    const count = countOccurrences(null, 1);
    console.log(count);
} catch (error) {
    console.log(error.message)
}

function countOccurrences(array, searchElement){
    if (!Array.isArray(array))
        throw new Error('不是數組')
    return array.reduce((a,b)=>{
        const occurrence = ( b === searchElement) ? 1: 0;
        return a+b ;  
    },0);
}