// Function Declaration
// 函式宣告
function mathFun(){
    console.log('run');
};
// Anonymous Function Expression
// 匿名函式表達式
const run = function (){
    console.log('ab');
}
// named Function Expression
const runRun = function mathFunFun(){
    console.log('cd');
}
let move = run;
mathFun();
run();
runRun();
move();
