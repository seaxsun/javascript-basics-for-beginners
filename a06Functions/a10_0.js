// method -> obj
// function -> global ( windows, global)

const video = {
    title:'a',
    play(){
        console.log(this);
    }
}

video.stop = function() {
    console.log(this);
}

function playVideo(){
    console.log(this);
}

//constructor
function aVideo(title){
    this.title = title;
    console.log(this);
}

const v = new aVideo('a'); // new empty obj like this {}

aVideo('a');

playVideo();

video.play();
video.stop();
