function interest(principal, rate, years){
    //設定預設值
    rate = rate || 3.5; 
    years = years || 5; 
    return principal * rate / 100 * years;
}
console.log(interest(10000));
//每年要還的錢，將是「1750」元
function interest2(principal, rate = 3.5, years){
    return principal * rate / 100 * years;
}
console.log(interest2(10000, undefined, 5));
//undefined 也是很好用


