const circle = {
    radius: 1,
    draw(){
        console.log('draw');
    }
};

const another = {};

for (let key in circle)
    another[key] = circle[key];
    
console.log(another);

// 類似上面的效果
const another1 = Object.assign({color: 1}, circle);
console.log(another1);

// 最簡單clone Object 的方法
const another2 = { ...circle};
console.log(another2);