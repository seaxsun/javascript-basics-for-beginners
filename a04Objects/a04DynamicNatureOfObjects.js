const circle = {
    radius : 1
};

//增加物件屬性
circle.color = 'yellow' ; 
circle.draw = function(){};

console.log(circle);

//刪除物件屬性
delete circle.color;
delete circle.draw;

console.log(circle);
