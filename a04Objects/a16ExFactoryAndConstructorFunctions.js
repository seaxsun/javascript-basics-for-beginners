function showAddress1(street, city, zipCode){
    return {
        street,
        city,
        zipCode,
        showAdd(){
           return `${this.zipCode}${this.city}${this.street}`;
        }
    }
}

let another1 = showAddress1('太和街', '新北市', '220');
console.log(another1);


function showAddress2(street, city, zipCode){
    this.street = street;
    this.city = city;
    this.zipCode = zipCode;
    this.showAdd = function(){
        return `${this.zipCode}${this.city}${this.street}`;
    };
}

let another2 = new showAddress2('太和街', '新北市', '220');
console.log(another2);
