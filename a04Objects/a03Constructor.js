//Constructor Function
function Circle(radius) {
    this.radius = radius,
    this.draw = function(){
        console.log('draw');
    }
}

//建構函數少了一層物件，所以要用 new 來升級為 object
const circle = new Circle(1);
console.log(circle) ; 

