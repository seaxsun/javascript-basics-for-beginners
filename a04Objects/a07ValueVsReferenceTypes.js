let x = 10;
let y = x; 
x = 20;
/* 
x = 20;
y = 10
*/


let a = {value: 10 };
let b = a ;
a.value = 20;
/* 
a{value : 20}
b{value : 20}
*/