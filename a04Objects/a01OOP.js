// Object-oriented Programming (OOP)
// 物件導向

const circle = {
    radius: 1,
    Location: {
        x:1,
        y:1
    },
    draw: function(){
        console.log('firstDraw');
    },
}

circle.draw(); // Method
//firstDraw


const circle2 = {
    radius: 2,
    draw: function(){
        console.log('secendDraw');
    },
}
circle2.draw(); // Method
console.log(circle2); // Object {radius: 2, draw: } 物件導向

/* 我 是 分 隔 線 */
/* 正在變成工廠函數之前 */
function createCube(){
    const cube = {
        radius: 2,
        draw: function(){
            console.log('secDraw');
        },
    }
    return cube
}
console.log(createCube());
/* Object {radius: 2, draw: } */ // 物件導向

/* 我 是 分 隔 線 */
/* 
let draw = function () {
    console.log('願主保佑我');
}
draw();

//願主保佑我
function drawCube() {
    console.log('願主保佑我');
}
drawCube();
 */
/* 願主保佑我 */

/* 我 是 分 隔 線 */
function cubes(radius){
     return {
        radius,
        draw(){
            console.log('secDraw');
        },
    }
}
const cube = cubes(2);
console.log(cube);
/* Object {radius: 2, draw: } */ //物件導向