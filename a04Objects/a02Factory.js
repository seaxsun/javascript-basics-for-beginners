//Factory Function
function createCircle (radius) {
    return {
        radius,
        draw() {
            console.log( 'draw' );
        }
    }
}
let circle = createCircle(1) ; 
console.log( circle );
let circle2 = createCircle(2);
console.log( circle2 );

// Factory Function
// 原本的工廠寫法
function createCircle2(radius){
    return{
        radius: radius,
        draw: function(){
            console.log('draw');
        }
    }
}
