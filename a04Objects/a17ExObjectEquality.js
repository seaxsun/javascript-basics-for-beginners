let address1 = new Address('a', 'b', 'c');
let address2 = new Address('a', 'b', 'c');
let address3 = address1 ;

//值相等嗎？ //true
console.log(areEqual(address1, address2));
//同參考物件嗎？ // false
console.log(areSame(address1, address2));
//同一個參考物件嗎？ // true
console.log(areSame(address1, address3));

//Constructor Function
function Address(street, city, zipCode){
    this.street = street;
    this.city = city;
    this.zipCode = zipCode;
}

function areEqual(address1, address2){
    return address1.street === address2.street &&
    address1.city === address2.city && 
    address1.zipCode === address2.zipCode;
}

function areSame(address1, address2){
   return address1 === address2;
}

