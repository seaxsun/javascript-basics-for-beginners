const first = [1,2,3];
const second = [4,5,6];
const combined = first.concat(second);
console.log(combined);
// Array(6) [1, 2, 3, 4, 5, 6]

let slice = combined.slice()
// slice() 沒有參數就是拷貝
console.log(slice);
// Array(6) [1, 2, 3, 4, 5, 6]

let slice2 = combined.slice(2);
//從位置2之後全要
console.log(slice2);
// Array(4) [3, 4, 5, 6]

let slice3 = combined.slice(2,4);
//從位置包含2，到位置不含4之間的數字
console.log(slice3);
// Array(2) [3, 4]

