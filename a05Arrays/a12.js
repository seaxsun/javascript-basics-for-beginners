const numbers = [2,3,1];
const another = numbers;

numbers.sort((a,b)=> a-b);
// a - b => -1 代表 a 放前面
// a - b => 1  代表 a 放後面
// a - b === 0 代表 a 放前面

console.log(numbers);
// Array(3) [1, 2, 3]
numbers.reverse();
console.log(numbers);
//Array(3) [3, 2, 1]
another.sort();
console.log(another);
//Array(3) [1, 2, 3]
//空白默認就是從小到大
