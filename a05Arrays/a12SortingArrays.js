const courses = [
    {id : 1, name : 'Node.js'},
    {id : 2, name : 'JavaScript'}
];

courses.sort( function(a,b){
    // a < b => -1   // a放前面
    // a > b => 1    // a放後面
    // a === b => 0  // a放前面
    if(a.name < b.name) return -1;
    if(a.name > b.name) return 1;
    return 0;
    // 非數字就用ASCII 的值大小來做判定
});

console.log(courses);