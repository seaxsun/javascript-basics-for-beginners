const numbers = [1,2,3,4];

// End
const last = numbers.pop();
console.log(numbers);
//Array(3) [1, 2, 3]
console.log(last);
//4

//Beginning
const start = numbers.shift();
console.log(numbers);
//Array(2) [2, 3]
console.log(start);
//1

//Middle
const middle = numbers.splice(1,1,5,5,5);
//在3之前加入5,5,5，並且移除1個數字3
console.log(numbers);
// Array(4) [2, 5, 5, 5]
console.log(middle);
// 3


