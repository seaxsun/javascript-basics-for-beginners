const movies = [
    {電影名:'5小雞雞', 年代:2018, 評分:5},
    {電影名:'4.5大雞雞', 年代:2018, 評分:4.5},
    {電影名:'4大小雞', 年代:2018, 評分:4},
    {電影名:'3.5雞大大', 年代:2018, 評分:3.5},
    {電影名:'雞小小', 年代:2016, 評分:3}
]
// All the movies in 2018 with rating > 4
// sort them by their rating
// Descending order
// Pick their title
// 超過3星，小於6星（改過）
// 排序遞減評分
// 只要他們電影名稱
const serch = movies
    .filter(n => n.年代 === 2018)
    .filter(n => n.評分>3 && n.評分< 6)
    .sort((a,b) => a.評分 > b.評分)
    .reverse()
    .map(n => n.電影名)
    .join('\n');
console.log(serch);

