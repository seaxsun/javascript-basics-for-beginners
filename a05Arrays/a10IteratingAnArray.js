const numbers = [1,2,3];
//一般我們都使用 of
for (let key of numbers)
    console.log(key);

//但也有些教程會使用 forEach
// forEach 有callback
numbers.forEach(n => console.log(n));
// 1
// 2
// 3

// 這裡的方法有寫
// 數值1是值，數值2是索引，數值3是array
numbers.forEach((a, b, c) => console.log(b,a,c ));
//0 1
//Array(3) [1, 2, 3]
//1 2
//Array(3) [1, 2, 3]
//2 3
//Array(3) [1, 2, 3]
