const numbers = [1,-1,2,3];

//方法1
let sum = 0;
for (let n of numbers)
    sum += n;
console.log(sum);

//方法2
const sum2 = numbers.reduce((sum, n) => sum+n ,0);
console.log(sum2);
// 這裡的 sum 代表的是上一個值，所以不需要 sum += n


