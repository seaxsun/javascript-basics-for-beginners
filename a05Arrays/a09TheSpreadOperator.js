const first = [1,2,3];
const second = [4,5,6];
//ES6 的spread運算式

// const combined = first.concat(second);
const combine = [...first,'a', 'b', ...second];
console.log(combine);
//Array(8) [1, 2, 3, "a", "b", 4, 5, 6]

//const copy = combined.slice()
const copy = [...combine];
console.log(copy);
// Array(8) [1, 2, 3, "a", "b", 4, 5, 6]



