//要重新指定Array 不能使用 const
let numbers = [1,2,3,4];
let another = numbers ;
let num = numbers;  
let num2 = num ; 
//another 如果沒引用 1,2,3,4 就會被自動丟掉
console.log(another);
//Array(4) [1, 2, 3, 4]

// Solution 1
numbers = [];
console.log(numbers);
//Array(0) []

// Solution 2
another.length = 0;
console.log(another);
// Array(0) []

// Solution 3
num.splice(0, num.length);
console.log(num);
// Array(0) []

// solution 4
while (num2.length > 0)
    num2.pop();
console.log(num2);
// Array(0) []
