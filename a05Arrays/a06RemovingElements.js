const numbers = [1,2,3,4];

// End
//numbers.push() //這裡是加入
/* const last = numbers.pop(); 
//這裡是彈掉
console.log(numbers);
console.log(last); */
//Array(3) [1, 2, 3]
//4

//Beginning
//numbers.unshift()
/* const first = numbers.shift();
console.log(numbers);
console.log(first);
 */

//Middle
const middle = numbers.splice(2,1);
console.log(numbers);
console.log(middle);
//從第二個開始彈掉一個，所以彈掉3


