const numbers = [1, -2 ,3];

//erery() 全部的數字有沒有符合，回傳布林值
//some() 部份有沒有符合

//every()
const allPositive = numbers.every(function(value){
    return value >= 0;
});
console.log(allPositive);
//false

//some()
const atLeastOnePositive = numbers.some(function(value){
    return value >= 0;
})
console.log(atLeastOnePositive);
//true