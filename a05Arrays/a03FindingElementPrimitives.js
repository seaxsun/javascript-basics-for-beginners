const numbers = [1,2,3,1,4];
//從前面找 //0
console.log(numbers.indexOf(1));
//從後面找 //3 （位置從前面算）
console.log(numbers.lastIndexOf(1));
//問有沒有 // true
console.log(numbers.includes(1));
// 配合邏輯運算符 //true
console.log(numbers.indexOf(1) !== -1);
//從位置2開始找1 //3
console.log(numbers.indexOf(1,2));
//找不到會回傳 -1 //-1
console.log(numbers.indexOf('a'));
