const courses= [
    { id :1 , name : 'a'},
    { id :2, name : 'b'}
];

//會像index 找回位置，或是-1
const key = courses.findIndex(function(courses){
    return courses.name === 'b';
});

console.log(key);

//會找回個物件 沒找到會回傳 underfined
const key2 = courses.find(function(courses){
    return courses.name === 'd';
});

console.log(key2);