const numbers = [1,-1,2,3];

//filter() 回傳每一個符合的值回來
const filtered = numbers.filter(n => n >= 0);
console.log(filtered);
//適合用在網站過濾商家營業時間
