const movies = [
    { title : 'a', year : 2018, rating :  1 },
    { title : 'b', year : 2017, rating :  3 },
    { title : 'c', year : 2018, rating :  5 },
    { title : 'd', year : 2017, rating :  6 },
    { title : 'e', year : 2018, rating :  2 },
    { title : 'f', year : 2018, rating :  4 },
    { title : 'g', year : 2018, rating :  3 },
    { title : 'h', year : 2018, rating :  3 },
    { title : 't', year : 2018, rating :  4 },
]

const items = movies
    //拿出2018
    .filter(n => n.year === 2018 && n.rating >= 3)
    //降排序
    .sort((a, b) => {return (a.rating - b.rating)})
    .reverse()
    //取得電影名字
    .map(obj => obj.title);
    
console.log(items.join("\n"));

//所有2018年電影的評分
//排序評分
//降序排列
//取得電影名稱