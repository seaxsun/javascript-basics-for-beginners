'use strict';

// 照理，應只有show 出draw 和 radius
// 但vscode 卻會透出所有公共插口
// 使用 嚴謹模式 use strict
// vscode 就不會再顯示出來
function Circle(radius) {
    // 利用 let 出去就死的特性，把屬性私有化
    this.radius = radius ; 
    let defaultLocation = { x: 5, y: 0 };

    //這行是不需要秀出來的
    let computerOptimumLocation = function(factor){
    };

    //真正該秀的是這行
    this.draw = function(){
        computerOptimumLocation(0.1);
        console.log('draw');
    }

    
}
const circle = new Circle(10);
circle.draw();
//這裡可以直接打 circle. 就會跑出公共接口