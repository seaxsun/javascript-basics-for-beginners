//程式目的為列舉屬性

function Circle(radius){
    this.radius = radius;
    this.draw = function(){
        console.log('draw');
    }
}

const circle = new Circle(10);

//只打印出屬性
//radius 10
for (let key in circle){
    if (typeof(circle[key]) !== 'function') 
        console.log(key, circle[key]);
}

//另一個作法, 他會把他列舉成 Array
//Array(2) ["radius", "draw"]
const keys = Object.keys(circle);
console.log(keys);

//使用 in 運算符
//circle has a radius.
if ('radius' in circle)
    console.log('circle has a radius.')
