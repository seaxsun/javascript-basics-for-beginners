
function Circle(radius) {
    this.radius = radius ;     
    
    //因為defaultLocation 被私有化
    //靠getDefaultlLocation 方法可以調用
    //但是()太醜，可以借用 get 和 set
    let defaultLocation = { x: 5, y: 0 };
    //Object.defineProperty(obj, prop, descriptor)
    //這裡就是設定取得與設定的地方
    //雖然這是建構寫法，但 Object.這裡的寫法要像工廠模式
    Object.defineProperty(this, 'getDefaultLocation',{
        get: function(){
            return defaultLocation ;
        },
        set: function(value){
            if (!value.x || !value.y)
                throw new Error('Invalid Location.')
            //如果物件的 x 或 y 缺少任一，則為假，就丟出Error
            defaultLocation = value// 這裡指定 object
        } 
    });
        this.draw = function(){
        computerOptimumLocation(0.1);
        console.log('draw');
    };
}
const circle = new Circle(10);
console.log(circle.getDefaultLocation);
//Object {x: 5, y: 0}
