function Circle(radius) {
    this.radius = radius ; 
    this.defaultLocation = { x: 5, y: 0 };

    //這行是不需要秀出來的
    this.computerOptimumLocation = function(factory){
        //....
    };

    //真正該秀的是這行
    this.draw = function(){
        this.computerOptimumLocation(0.1);
        console.log('draw');
    }
}
const circle = new Circle(10);
circle.draw();