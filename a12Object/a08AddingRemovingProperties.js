function Circle(radius){
    this.radius = radius || 2 ; 
    this.draw = () => {
        console.log('draw', this.radius);
    }

}

const another = new Circle ;
another.draw();

another.radius = 4;

console.log(another.radius);