// 位元運算符 Bitwise Operators
// 1 = 00000001
// 2 = 00000010
// 3 = 00000011
// 4 = 00000100
// 5 = 00000101
// 6 = 00000110
// R = 00000000

console.log(1|2); // Bitwise OR
// 3
console.log(1&2); // Bitwise AND
// 0

// read , write , execute
const readPermission = 4 ;
// 4 = 00000100
const writePermission = 2;
// 2 = 00000010
const executePermission = 1;
// 1 = 00000001

let myPermission = 0 ; 
myPermission = myPermission | writePermission ; 
console.log('myPermission',myPermission);
// 2
let message = (myPermission | readPermission) ? 'yes' : 'no' ; 
// | -> 6 -> yes // & -> 0 -> no
console.log('message',message);
