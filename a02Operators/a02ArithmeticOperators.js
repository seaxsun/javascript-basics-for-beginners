// 算術運算符
// Arithmetic Operators
let x = 10; 
let y = 5;
let a = 2 ; 
let b = 5 ;
let c = 10 ; 
let z = 10 ; 
console.log(x + y); //15
console.log(x - y); //5
console.log(x * y); //50
console.log(x / y); //2
console.log(x % y); //0
console.log(a ** b);  //32 ; 2的5次方

// Increment (++)

console.log(++c); //加完就打印出來 11
console.log(z++); //還沒增量就被打印出來 10
console.log(z); //這裡的 z是已經加完的 11

// Decrement (--)
console.log(--x); //同上
