// 三元運算符
// Ternary Operator
// 客戶如果得分超過100分
// 他們是黃金客戶
// 否則 他們是白銀客戶

/* 
If a customer has more than 100 points,
they are a 'gold' customer, otherwise,
they are a 'silver' customer.
*/

let points = 110 ; 
let type = '';
(points > 100) ? console.log(type = 'Gold') : console.log(type = 'Silver'); 

let points2 = 110 ; 
let type2 = points2 > 100 ? 'Gold' : 'Silver';
console.log(type2);

