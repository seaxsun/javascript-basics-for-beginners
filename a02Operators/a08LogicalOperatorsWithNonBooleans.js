// 邏輯運算符  Logical Operators
console.log(false || true);
console.log(false || 'Mosh');
console.log(true || 'Mosh');
console.log(false || 1 );
console.log(false || '' );
//short - circuiting
console.log(false || 1 || 2 );

//買衣服範例
let userColor = '' ; 
let defaultColor = 'blue';
let currentColor = userColor || defaultColor ; //未來可以用在預設選色
console.log(currentColor);

// Falsy (false)
// undefined
// null
// 0 
// false
// ''
// NaN
// Anything that is not Falsy -> Truthy
