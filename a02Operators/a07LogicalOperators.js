// 邏輯運算符
// Logical AND(&&)
// Returns TRUE if both operands are TRUE

console.log(true && true);
console.log(false && true);

let highIncome = true ; // 高收入
let goodCreditScore = true ; // 信用分數高
let eligibleForLoan = highIncome && goodCreditScore ; //符合貸款條件
console.log(eligibleForLoan);

// Logical OR (||)
// Returns TRUE if one of the operand is TRUE
let highIncome2 = false ; // 高收入
let goodCreditScore2 = true ; // 信用分數高
let eligibleForLoan2 = highIncome2 || goodCreditScore2 ; //符合貸款條件
console.log('Eligible',eligibleForLoan2); // 有資格獲得貸款

// NOT (!)
let applicationRefused = !eligibleForLoan2 ;
console.log('Application Refused',applicationRefused) ; 
