// 比較運算符
// Comparison Operators

let x = 1 ; 
const y = '1';

// Relational 關係型
console.log( x > 0 ); //true
console.log( x >= 1); //true
console.log( x < 1); //false
console.log( x <= 1); //true
console.log( 1 == '1') //true

// Equality 平等
console.log(x === 1); //true
console.log(x !== 1); //false
console.log(1 === '1'); //false

