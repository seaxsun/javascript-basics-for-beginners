// 分配運算符
// Assignment Operators
let x = 10;
let y = 10;

x = x + 5;
y += 5 ; 
console.log(x);
console.log(y);

x = x * 3 ; 
y *= 3 ; 
console.log(x);
console.log(y);
