// 平等運算符
// Equality Operators

// Strict Equality (Type + Value)
console.log( 1 === '1');
console.log( true === '1');
console.log( '1' === true);

// Lose Equality
console.log( 1 == "1");
console.log( true == "1");
console.log( '1' == true);
// 辨視 true 如果裡頭是空字串，就是 false 

