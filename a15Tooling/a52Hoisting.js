// function 會被 Hoisting 起來
sayHello();
//sayGoodbye();

// function Declaration
function sayHello(){
    console.log('Hello');
}

// function expression
const sayGoodbye = function(){
    console.log('Goodbye');
}


// 目前類並沒有被 Hoisting
// 會出錯
const cb = new Circle();

// 目前大家都是使用這個
// class declaration
class Circle {}

// class expression
const Square = class{}