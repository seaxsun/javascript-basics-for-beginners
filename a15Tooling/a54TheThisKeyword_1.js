class Circle {
    draw(){
        console.log(this)
    }
}

const c = new Circle();
const draw = c.draw;
draw();

//如果使用了 class 將會直接是 嚴格模式
//class 預設是在 strip mode 下執行
//防止更動到全局物件