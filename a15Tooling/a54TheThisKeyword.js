'use strict';

const Circle = function() {
    this.draw = function(){ console.log(this);}
}

const c = new Circle();//這段為 Method Call
c.draw();//正常的物件導向

const draw = c.draw;
draw();//變成全局的方法
//當上方有 use strict 使用嚴格方法，就無法全局使用
//將會變成「 undefined 」

