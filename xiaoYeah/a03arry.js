const fruitsArr = [
    {name: 'Melon', seasonal: true, price:'2020'},
    {name: 'Strawberry', seasonal: false, price: '120'},
    {name: 'Banana', seasonal: true, price: '40'},
    {name: 'Blue Berry', seasonal: false, price: '300'},
    {name: 'Mango', seasonal: true, price: '100'},
    {name: 'Pinapple', seasonal: true, price: '80'},
    {name: 'Cherry tomato', seasonal: true, price: '70'},
]

const superMarket = fruitsArr
    .filter(n => n.seasonal === true)
    .map(n => n.name);
const jamFactory = fruitsArr
    .filter(n => n.seasonal === false)
    .map(n => n.name);

console.log(superMarket);
//console.log(b);
